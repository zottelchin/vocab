import builtins from "rollup-plugin-node-builtins";
import autoInstall from "@rollup/plugin-auto-install";
import nodeResolve from "@rollup/plugin-node-resolve";
import commonjs from "@rollup/plugin-commonjs";
import replace from "@rollup/plugin-replace";
import json from "@rollup/plugin-json";
import html2 from "rollup-plugin-html2";
import postcss from "rollup-plugin-postcss";
import copy from "rollup-plugin-copy";
import vue from "rollup-plugin-vue";
import process from "process";

export default {
	input: "main.js",
	output: {
		dir: "dist",
		format: "iife"
	},
	plugins: [
		replace({
			"process.env.NODE_ENV": JSON.stringify(process.env.NODE_ENV),
		}),
		builtins(),
		postcss({
			plugins: [],
			extract: true,
		}),
		nodeResolve({}),
		commonjs(),
		json(),
		vue(),
		autoInstall(),
		copy({
			targets: [
				{src: ["lib/**"], dest: "dist/lib"},
			],
			copyOnce: true,
			flatten: false,
		}),
		copy({targets: [{src: "node_modules/remixicon/fonts/*", dest: "dist"}], copyOnce: true, flatten: true}),
		html2({
			title: "Vocab",
			meta: {
				viewport: "width=device-width, initial-scale=1",
			},
			fileName: "dist/index.html",
			template: "<!doctype html><html><head><meta charset=\"utf-8\"></head><body><div id=\"app\"></div></body></html>",
			onlinePath: "/"
		}),
	]
};
