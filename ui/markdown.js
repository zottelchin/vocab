import MarkdownIt from "markdown-it";
import MarkdownItTexMath from "markdown-it-texmath";
import Katex from "katex";
import "katex/dist/katex.min.css";
import "markdown-it-texmath/css/texmath.css";

const markdown = {
	linkify: true,
	breaks: true,
};

const mdit = new MarkdownIt(markdown).use(MarkdownItTexMath, {
	engine: Katex,
	delimiters: "dollars",
});

export default function md() {
	return mdit.render(...arguments);
}
