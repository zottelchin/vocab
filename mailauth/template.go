package mailauth

import (
	"bytes"
	"fmt"
	"golang.org/x/text/language"
	htmlTemplate "html/template"
	"strings"
	textTemplate "text/template"
)

// See https://emailframe.work/
var TemplateHTMLHeader = `<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="en">
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title></title>
  <style type="text/css">
    .button {
      display: inline-block; display: inline-flex;
      font-weight: bold; text-decoration: none;
      background-color: #3298dc; border: 1px solid transparent; color: #fff;
      cursor: pointer; white-space: nowrap; text-align: center;
      align-items: center; justify-content: center;
      height: 2.5em; line-height: 1.5;
      margin-top: 1em; margin-bottom: 0.5em;
      padding-bottom: 0.5em; padding-bottom: calc(0.5em - 1px); padding-left: 1em; padding-right: 1em; padding-top: 0.5em; padding-top: calc(0.5em - 1px);
      border-radius: 4px; box-shadow: none;
      -moz-appearance: none; -webkit-appearance: none;
      position: relative; vertical-align: top;
    }
    .button:hover, .button:focus { background-color: #2793da; }
    .button:active { background-color: #238cd1; }
    a { color: #3273dc }
  </style>
</head>
<body style="margin: 0; padding: 0; color: #4a4a4a; font-weight: 400; line-height: 1.5; font-family: BlinkMacSystemFont, -apple-system, 'Segoe UI', 'Roboto', 'Oxygen', 'Ubuntu', 'Cantarell', 'Fira Sans', 'Droid Sans', 'Helvetica Neue', 'Helvetica', 'Arial', sans-serif;"><center>
<table width="100%" border="0" cellpadding="0" cellspacing="0">
<tr><td align="left" valign="top" style="padding: 3rem 1.5rem;">
{{if .ServiceName}}<h1 style="font-size: 3em; padding: 0; margin: 0; margin-bottom: 1.5rem; color: #363636; font-weight: 600; line-height: 1.125;">{{.ServiceName}}</h1>{{end}}`
var TemplateHTMLFooter = `</td></tr></table></center></body></html>`

// InternationalizedTemplateContent contains the email templates in various languages with the first line being the subject.
// For HTML templates, it will be surrounded by TemplateHTMLHeader and TemplateHTMLFooter, and .HTML will be true.
var InternationalizedTemplateContent = map[language.Tag]string{
	language.English: "Please confirm your login{{if .ServiceName}} at {{.ServiceName}}{{end}}\n" +
		"{{if .HTML}}<strong>{{end}}You recently requested to log in with this email address at " +
		"{{if .ServiceName}}{{if .HTML}}<a href=\"{{.RootURL}}\">{{end}}{{.ServiceName}}{{if .HTML}}</a>{{else}} ({{.RootURL}}){{end}}" +
		"{{else}}{{if .HTML}}<a href=\"{{.RootURL}}\">{{.RootURL}}</a>{{else}}{{.RootURL}}{{end}}{{end}}.{{if .HTML}}</strong>{{end}}<br>\n" +
		"{{if .HTML}}Please confirm your login:<br>\n" +
		"<a href=\"{{.Link}}\" class=\"button\">Confirm login</a><br>\n" +
		"<small><a href=\"{{.Link}}\"><pre style=\"white-space: pre-wrap; word-break: break-all;\">{{.Link}}</pre></a></small>" +
		"{{else}}Please open the following link to log in: {{.Link}}\n{{end}}" +
		"<br>\n" +
		"If you haven't requested access to this service yourself or don't know what this email is about, you can safely ignore it.",
	language.German: "Bitte verifiziere deine Anmeldung{{if .ServiceName}} bei {{.ServiceName}}{{end}}\n" +
		"{{if .HTML}}<strong>{{end}}Sie haben kürzlich eine Anmeldung mit dieser E-Mail-Adresse bei " +
		"{{if .ServiceName}}{{if .HTML}}<a href=\"{{.RootURL}}\">{{end}}{{.ServiceName}}{{if .HTML}}</a>{{else}} ({{.RootURL}}){{end}}" +
		"{{else}}{{if .HTML}}<a href=\"{{.RootURL}}\">{{.RootURL}}</a>{{else}}{{.RootURL}}{{end}}{{end}} beantragt.{{if .HTML}}</strong>{{end}}<br>\n" +
		"{{if .HTML}}Bitte bestätigen Sie Ihre Anmeldung:<br>\n" +
		"<a href=\"{{.Link}}\" class=\"button\">Anmeldung bestätigen</a><br>\n" +
		"<small><a href=\"{{.Link}}\"><pre style=\"white-space: pre-wrap; word-break: break-all;\">{{.Link}}</pre></a></small>" +
		"{{else}}Bitte öffnen Sie den folgenden Link, um sich anzumelden: {{.Link}}\n{{end}}" +
		"<br>\n" +
		"Wenn Sie keinen Zugriff auf diesen Dienst angefordert haben oder nicht wissen, worum es in dieser E-Mail geht, können Sie sie gefahrlos ignorieren.",
}

type TemplateContext struct {
	ServiceName  string
	RootURL      string
	EndpointPath string
	Token        string
	Mail         string
	Link         string
	HTML         bool
}

var availableLanguages []language.Tag
var renderedTextTemplates = map[language.Tag]*textTemplate.Template{}
var renderedHTMLTemplates = map[language.Tag]*htmlTemplate.Template{}

func getBestLanguage(languageHeader string) language.Tag {
	if availableLanguages == nil {
		availableLanguages = make([]language.Tag, 0, len(InternationalizedTemplateContent))
		for k := range InternationalizedTemplateContent {
			availableLanguages = append(availableLanguages, k)
		}
	}
	speaker, _, err := language.ParseAcceptLanguage(languageHeader)
	if err != nil {
		speaker = []language.Tag{}
	}
	_, i, _ := language.NewMatcher(availableLanguages).Match(speaker...)
	return availableLanguages[i]
}

func GetTemplates(languageHeader string) (*textTemplate.Template, *htmlTemplate.Template) {
	lang := getBestLanguage(languageHeader)
	if _, ok := renderedTextTemplates[lang]; ok {
		return renderedTextTemplates[lang], renderedHTMLTemplates[lang]
	}
	var err error
	renderedTextTemplates[lang], err = textTemplate.New("mailauth-text-" + lang.String()).Parse(strings.ReplaceAll(InternationalizedTemplateContent[lang], "<br>", ""))
	if err != nil {
		fmt.Printf("Panicking due to template parsing error for mailauth-text-%s\n", lang.String())
		panic(err)
	}
	renderedHTMLTemplates[lang], err = htmlTemplate.New("mailauth-html-" + lang.String()).Parse(
		TemplateHTMLHeader +
			InternationalizedTemplateContent[lang][strings.Index(InternationalizedTemplateContent[lang], "\n")+1:] +
			TemplateHTMLFooter,
	)
	if err != nil {
		fmt.Printf("Panicking due to template parsing error for mailauth-html-%s\n", lang.String())
		panic(err)
	}
	return renderedTextTemplates[lang], renderedHTMLTemplates[lang]
}

func ExecuteTemplates(languageHeader string, templateContext TemplateContext) (string, []byte, []byte) {
	textMailTemplate, htmlMailTemplate := GetTemplates(languageHeader)

	textMailContent := new(bytes.Buffer)
	err := textMailTemplate.Execute(textMailContent, templateContext)
	if err != nil {
		fmt.Printf("Panicking due to template execution error for %s with context %+v\n", textMailTemplate.Name(), templateContext)
		panic(err)
	}
	textMailParts := bytes.SplitN(textMailContent.Bytes(), []byte("\n"), 2)

	htmlMailContent := new(bytes.Buffer)
	templateContext.HTML = true
	err = htmlMailTemplate.Execute(htmlMailContent, templateContext)
	if err != nil {
		fmt.Printf("Panicking due to template execution error for %s with context %+v\n", htmlMailTemplate.Name(), templateContext)
		panic(err)
	}

	return string(textMailParts[0]), textMailParts[1], htmlMailContent.Bytes()
}
