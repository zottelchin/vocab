package mailauth

import (
	"crypto/rand"
	"crypto/tls"
	"fmt"
	"github.com/gin-gonic/gin"
	"github.com/jordan-wright/email"
	"github.com/ulule/limiter/v3"
	mgin "github.com/ulule/limiter/v3/drivers/middleware/gin"
	"github.com/ulule/limiter/v3/drivers/store/memory"
	"net/smtp"
	"net/url"
	"strings"
	"time"
)

type Encryption string

const (
	EncryptWithTLS              Encryption = "tls"
	EncryptWithStartTLS         Encryption = "starttls"
	EncryptWithInsecureTLS      Encryption = "insecure-tls"
	EncryptWithInsecureStartTLS Encryption = "insecure-starttls"
	EncryptNotAtAll             Encryption = ""
)

// MailAuth provides a simple passwordless authentication service using only email validation, and should only be used in low-security applications.
type MailAuth struct {
	MailServer         string    // SMTP server address (e.g. smtp.postmarkapp.org:587)
	MailAccount        smtp.Auth // SMTP authentication details (e.g.
	MailEncryption     Encryption
	MailFrom           string
	ServiceName        string
	RootURL            string
	EndpointPath       string
	OnLogin            func(string) error
	AllowAtInvalid     bool
	CookieMaxAge       time.Duration
	CookiePath         string
	CookieDomain       string
	CookieSecure       bool
	CookieHTTPOnly     bool
	VerificationMaxAge time.Duration
}

// TokenLength specifies the length of the session & verification tokens in bytes.
var TokenLength = 64

// verificationTokens is a map from email to verification token & expiration date, thus only one verification token can exist for each email address.
var verificationTokens = map[string]*ExpiringContent{}

// sessionTokens is a map from session token to email & expiration date, thus multiple active tokens can exist for the same email address.
var sessionTokens = map[string]*ExpiringContent{}

// Setup creates the routes on *MailAuth.EndpointPath using the given Gin router.
func (m *MailAuth) Setup(r gin.IRoutes) {
	// Default expiration durations
	if m.CookieMaxAge.Seconds() < 1 {
		m.CookieMaxAge = 24 * time.Hour
	}
	if m.VerificationMaxAge.Seconds() < 1 {
		m.VerificationMaxAge = 6 * time.Hour
	}
	m.RootURL = strings.TrimSuffix(m.RootURL, "/")

	// Rate limiter to prevent abuse
	checkLimiter := mgin.NewMiddleware(limiter.New(memory.NewStore(), limiter.Rate{
		Period: 10 * time.Second,
		Limit:  3,
	}))
	requestLimit := mgin.NewMiddleware(limiter.New(memory.NewStore(), limiter.Rate{
		Period: 15 * time.Minute,
		Limit:  5,
	}))

	// Add routes to Gin router
	r.GET(m.EndpointPath, checkLimiter, m.GET)
	r.POST(m.EndpointPath, requestLimit, m.POST)
	r.DELETE(m.EndpointPath, checkLimiter, m.DELETE)

	// Clean up every 15 minutes
	go func() {
		for {
			time.Sleep(15 * time.Minute)
			CleanupExpiredContents(sessionTokens)
			CleanupExpiredContents(verificationTokens)
		}
	}()
}

// GET can do two things:
// If the "token" and "email" query parameters are set (by clicking an email link), it sets the cookie "mailauth-token" to a valid session token, and then redirects the user to *MailAuth.RootURL.
// If it's not, it returns the result of *MailAuth.Authorize(c) as a JSON object with the format { "mail": "mail@example.org", "error": null }.
//
// Beware: this is a non-idempotent GET request (frowned upon when implementing a REST API) because it should work easily when clicking a link in an email!
func (m *MailAuth) GET(c *gin.Context) {
	if c.Query("token") != "" && c.Query("email") != "" {
		// email is non-empty, so this only returns true if the token exists & is correct
		if verificationTokens[c.Query("email")].Get() == c.Query("token") { // logged in
			// generate a session token
			sessionToken := make([]byte, TokenLength)
			n, err := rand.Read(sessionToken)
			if n < TokenLength || err != nil {
				c.String(500, "not enough entropy (the server probably failed catastrophically)")
				return
			}
			sessionTokenString := fmt.Sprintf("%x", sessionToken)
			sessionTokens[sessionTokenString] = &ExpiringContent{
				Content: c.Query("email"),
				Expired: time.Now().Add(m.CookieMaxAge),
			}

			// call the OnLogin hook and fail if it returns an error
			if m.OnLogin != nil {
				err := m.OnLogin(c.Query("email"))
				if err != nil {
					c.String(500, "%s", err.Error())
					return
				}
			}

			// clean up the verification token, set the cookie, redirect the user
			delete(verificationTokens, c.Query("email"))
			c.SetCookie("mailauth-token", sessionTokenString, int(m.CookieMaxAge.Seconds()), m.CookiePath, m.CookieDomain, m.CookieSecure, m.CookieHTTPOnly)
			c.Redirect(302, m.RootURL)
		} else {
			// unauthorized (wrong token)
			c.String(401, "invalid verification token provided (have you maybe already used it or is there a newer verification email?)")
		}
	} else {
		// return authentication details
		mail, err := m.Authenticate(c)
		if err != nil {
			c.JSON(401, gin.H{"email": nil, "error": err.Error()})
		} else {
			c.JSON(200, gin.H{"email": mail, "error": nil})
		}
	}
}

// POST requests a verification email for the address submitted in the body field "email".
func (m *MailAuth) POST(c *gin.Context) {
	// parse body
	body := struct {
		Mail string `json:"email"`
	}{
		c.PostForm("email"),
	}
	_ = c.ShouldBindJSON(&body)
	body.Mail = strings.TrimSpace(strings.ToLower(body.Mail))
	if body.Mail == "" || !strings.Contains(body.Mail, "@") {
		c.String(400, "missing or invalid email address")
		return
	}

	// generate verification token
	verificationToken := make([]byte, TokenLength)
	n, err := rand.Read(verificationToken)
	if n < TokenLength || err != nil {
		c.String(500, "not enough entropy (the server probably failed catastrophically)")
		return
	}
	verificationTokenString := fmt.Sprintf("%x", verificationToken)
	verificationTokens[body.Mail] = &ExpiringContent{
		Content: verificationTokenString,
		Expired: time.Now().Add(m.VerificationMaxAge),
	}

	// build the verification link
	link := m.RootURL + m.EndpointPath + "?email=" + url.QueryEscape(body.Mail) + "&token=" + url.QueryEscape(verificationTokenString)

	// allow @invalid email addresses immediately (demo mode or for privacy reasons)
	if m.AllowAtInvalid && strings.HasSuffix(body.Mail, "@invalid") {
		c.Request.URL.RawQuery = strings.SplitN(link, "?", 2)[1]
		m.GET(c)
		return
	}

	// prepare & send an email
	templateContext := TemplateContext{
		ServiceName:  m.ServiceName,
		RootURL:      m.RootURL,
		EndpointPath: m.EndpointPath,
		Token:        verificationTokenString,
		Mail:         body.Mail,
		Link:         link,
	}
	subject, textBody, htmlBody := ExecuteTemplates(c.GetHeader("Accept-Language"), templateContext)

	mail := email.NewEmail()
	mail.From = m.MailFrom
	mail.To = []string{body.Mail}
	mail.Subject = subject
	mail.HTML = htmlBody
	mail.Text = textBody
	if strings.HasSuffix(string(m.MailEncryption), "starttls") {
		err = mail.SendWithStartTLS(m.MailServer, m.MailAccount, &tls.Config{InsecureSkipVerify: strings.HasPrefix(string(m.MailEncryption), "insecure-")})
	} else if strings.HasSuffix(string(m.MailEncryption), "tls") {
		err = mail.SendWithTLS(m.MailServer, m.MailAccount, &tls.Config{InsecureSkipVerify: strings.HasPrefix(string(m.MailEncryption), "insecure-")})
	} else {
		err = mail.Send(m.MailServer, m.MailAccount)
	}
	if err != nil {
		fmt.Printf("couldn't send verification email: %s\n", err.Error())
		c.String(500, "couldn't send verification email due to an internal server error")
	} else {
		c.String(200, "verification email has been sent, please check your inbox")
	}
}

// DELETE requires authentication & manually deletes the current user's session token, and redirects the user to *MailAuth.RootURL.
func (m *MailAuth) DELETE(c *gin.Context) {
	token := c.GetHeader("Authorization")
	if token == "" || !strings.HasPrefix(token, "Bearer ") {
		token, _ = c.Cookie("mailauth-token")
	} else {
		token = strings.TrimPrefix(token, "Bearer ")
	}
	delete(sessionTokens, token)
	c.String(200, "logout was successful")
}
