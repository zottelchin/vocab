package mailauth

import "time"

// ExpiringContent provides a string value that is only valid until a specified timestamp, and is used internally to store tokens.
type ExpiringContent struct {
	Content string
	Expired time.Time
}

func (e *ExpiringContent) Get() string {
	if e == nil || e.Expired.Before(time.Now()) {
		return ""
	}
	return e.Content
}

func CleanupExpiredContents(contentMap map[string]*ExpiringContent) {
	now := time.Now()
	for k := range contentMap {
		if contentMap[k].Expired.Before(now) {
			delete(contentMap, k)
		}
	}
}
