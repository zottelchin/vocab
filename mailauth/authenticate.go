package mailauth

import (
	"errors"
	"github.com/gin-gonic/gin"
	"strings"
)

var ErrorMissingToken = errors.New("no token provided")
var ErrorInvalidToken = errors.New("invalid token")

// Authenticate returns the email address of the current user if they're logged in, or an error if they're not.
func (m *MailAuth) Authenticate(c *gin.Context) (string, error) {
	token := c.GetHeader("Authorization")
	if token == "" || !strings.HasPrefix(token, "Bearer ") {
		var err error
		token, err = c.Cookie("mailauth-token")
		if token == "" || err != nil {
			return "", ErrorMissingToken
		}
	} else {
		token = strings.TrimPrefix(token, "Bearer ")
	}
	if email := sessionTokens[token].Get(); email != "" {
		return email, nil
	}
	return "", ErrorInvalidToken
}
