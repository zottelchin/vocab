module codeberg.org/zottelchin/vocab

go 1.14

require (
	github.com/gin-gonic/gin v1.6.3
	github.com/jinzhu/gorm v1.9.16
	github.com/jordan-wright/email v0.0.0-20200602115436-fd8a7622303e
	github.com/lib/pq v1.7.1 // indirect
	github.com/markbates/pkger v0.17.0
	github.com/matoous/go-nanoid v1.4.1
	github.com/ulule/limiter/v3 v3.5.0
	golang.org/x/text v0.3.2
)
