# Vocab

## Getting Started

```bash
# 1. Get the repository
git clone https://codeberg.org/zottelchin/vocab.git && cd vocab

# 2. Start a database
docker run --rm -d -p 5432:5432 -e POSTGRES_DB=vocab -e POSTGRES_PASSWORD=geheim postgres:12-alpine

# 3. Install frontend dependencies
cd ui; npm install; cd .. 

# 4. Build the frontend
cd ui; npm run build; cd ..

# 5. Start the application
go run ./cmd/vocab
```

## Ensure correct code style
```
echo 'echo "> go fmt ./..." && go fmt ./... && cd ui && npm run clean && cd ..' > .git/hooks/pre-commit
chmod +x .git/hooks/pre-commit
```

This will be run whenever running `git commit`, and can be invoked manually using `.git/hooks/pre-commit`.  
Note that you'll have to amend your commit if you don't run the pre-commit hook manually before committing.
