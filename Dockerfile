##############
## FRONTEND ##
##############

FROM node:lts-alpine AS frontend

# Dependencies
COPY ui/package.json /app/ui/package.json
WORKDIR /app/ui
RUN npm install

# Build
COPY ui /app/ui
RUN npm run build

#############
## BACKEND ##
#############

FROM golang:latest AS backend

# pkger for packing UI files
RUN go get github.com/markbates/pkger/cmd/pkger
# https://github.com/markbates/pkger/issues/116#issuecomment-680161560
RUN ln -s /go /workaround_pkger_go_path && ln -s /root /workaround_pkger_home
ENV GOPATH /workaround_pkger_go_path
ENV HOME /workaround_pkger_home

# Backend Dependencies
COPY go.mod /app/go.mod
WORKDIR /app
RUN go mod download

# Build Backend
COPY *.go /app/
COPY cmd /app/cmd
COPY mailauth /app/mailauth
COPY internal /app/internal
COPY --from=frontend /app/ui/dist /app/ui/dist
RUN pkger list && pkger -o internal
RUN CGO_ENABLED=0 GOOS=linux GOARCH=amd64 go build -o vocab-server ./cmd/vocab

#################
## FINAL IMAGE ##
#################

FROM scratch
COPY --from=backend /app/vocab-server /bin/vocab-server
COPY --from=backend /etc/ssl /etc/ssl
ENTRYPOINT ["/bin/vocab-server"]
