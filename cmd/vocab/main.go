package main

import (
	"codeberg.org/zottelchin/vocab/internal"
)

func main() {
	internal.Start()
}
