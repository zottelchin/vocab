package internal

import (
	"encoding/base64"
	"fmt"
	"github.com/gin-gonic/gin"
	"github.com/jinzhu/gorm"
	"github.com/matoous/go-nanoid"
	"io/ioutil"
	"mime"
	"os"
	"path"
	"path/filepath"
	"strings"
	"time"
)

func Setup(r gin.IRoutes) {
	r.GET("/api/decks/", listDecks)
	r.PUT("/api/decks/:deck", createOrUpdateDeck)
	r.GET("/api/decks/:deck", getDeck)
	r.DELETE("/api/decks/:deck", deleteDeck)
	r.PUT("/api/decks/:deck/cards/:card", createQuestion)
	r.GET("/api/decks/:deck/next-card", getNext)
	r.POST("/api/decks/:deck/next-card", questionAnswered)
	r.POST("/api/attachments", upload)
	r.GET("/api/attachments/:name", download)

	go func() {
		for {
			cleanupAttachments()
			time.Sleep(1 * time.Minute)
		}
	}()
}

func upload(c *gin.Context) {
	mail, err := Authentication.Authenticate(c)
	if err != nil {
		c.Status(403)
		c.Abort()
		return
	}
	file, err := c.FormFile("upload")
	if err != nil {
		c.String(400, err.Error())
		return
	}
	mimeType := mime.TypeByExtension(path.Ext(file.Filename))
	if !strings.HasPrefix(mimeType, "image/") {
		c.String(415, "files must be of mime type image/* (got "+mimeType+")")
		return
	}
	if file.Size > 5*1024*1024 { // 5 MB
		c.String(413, "files must be at most 5 MiB in size")
		return
	}
	id, err := gonanoid.Nanoid(11)
	if err != nil {
		c.String(500, err.Error())
		return
	}
	userDirectory := base64.RawURLEncoding.EncodeToString([]byte(mail))
	_ = os.MkdirAll("./uploads/"+userDirectory, 0755)
	err = c.SaveUploadedFile(file, "./uploads/"+userDirectory+"/"+id+path.Ext(file.Filename))
	if err != nil {
		c.String(500, err.Error())
		return
	}
	c.JSON(200, gin.H{
		"name": id + path.Ext(file.Filename),
	})
}
func download(c *gin.Context) {
	mail, err := Authentication.Authenticate(c)
	if err != nil {
		c.Status(403)
		c.Abort()
		return
	}
	userDirectory := base64.RawURLEncoding.EncodeToString([]byte(mail))
	c.File("./uploads/" + userDirectory + "/" + filepath.Base(filepath.Clean(c.Param("name"))))
}
func cleanupAttachments() {
	fmt.Printf("Starting cleanup...\n")
	users, err := ioutil.ReadDir("./uploads")
	if err != nil {
		fmt.Printf("Cleanup failed: %s\n", err)
		return
	}
	for _, directory := range users {
		user, err := base64.RawURLEncoding.DecodeString(directory.Name())
		if err != nil {
			fmt.Printf("Couldn't decode base64 name: %s\n", err)
			continue
		}
		files, err := ioutil.ReadDir("./uploads/" + directory.Name())
		if err != nil {
			fmt.Printf("Couldn't list directory %s: %s\n", "./uploads/"+directory.Name(), err)
			continue
		}
		for _, file := range files {
			cond := "%/api/attachments/" + file.Name() + "%"
			query := db.Exec("SELECT decks.id FROM \"decks\" INNER JOIN \"questions\" ON decks.id = questions.deck_id WHERE decks.owner = ? AND (questions.question LIKE ? OR questions.answer LIKE ?)", user, cond, cond)
			if query.RecordNotFound() || query.RowsAffected < 1 {
				err = os.Remove("./uploads/" + directory.Name() + "/" + file.Name())
				if err != nil {
					fmt.Printf("Couldn't remove file %s: %s\n", "./uploads/"+directory.Name()+"/"+file.Name(), err)
				} else {
					fmt.Printf("Cleaned up: %s\n", "./uploads/"+directory.Name()+"/"+file.Name())
				}
			} else if query.Error != nil {
				fmt.Printf("Couldn't query database: %s\n", query.Error)
			}
		}
	}
	fmt.Printf("Cleanup done.\n")
}

func listDecks(c *gin.Context) {
	mail, err := Authentication.Authenticate(c)
	if err != nil {
		c.Status(403)
		c.Abort()
		return
	}
	decks := []Deck{}
	db.Find(&decks, Deck{Owner: mail})
	c.JSON(200, decks)
}

func createOrUpdateDeck(c *gin.Context) {
	mail, err := Authentication.Authenticate(c)
	if err != nil {
		c.Status(403)
		c.Abort()
		return
	}

	var count int
	db.Model(&Deck{}).Where(&Deck{
		ID:    c.Param("deck"),
		Owner: mail,
	}).Count(&count)

	deck := Deck{ID: c.Param("deck"), Owner: mail}
	c.BindJSON(&deck)

	if count == 0 {
		db.Create(&deck)
	} else {
		db.Model(&deck).Select("Name").Updates(&deck)
	}
}

func getDeck(c *gin.Context) {
	mail, err := Authentication.Authenticate(c)
	if err != nil {
		c.Status(403)
		c.Abort()
		return
	}
	deck := Deck{}
	db.Preload("Questions").First(&deck, Deck{Owner: mail, ID: c.Param("deck")})
	c.JSON(200, deck)
}

func createQuestion(c *gin.Context) {
	mail, err := Authentication.Authenticate(c)
	if err != nil {
		c.Status(403)
		c.Abort()
		return
	}
	deckID := c.Param("deck")
	cardsID := c.Param("card")
	deck := Deck{}
	Card := Question{}
	c.BindJSON(&Card)
	Card.ID = cardsID
	db.FirstOrCreate(&deck, &Deck{ID: deckID, Owner: mail})
	deck.Questions = append(deck.Questions, Card)
	db.Save(&deck)
}

func getNext(c *gin.Context) {
	_, err := Authentication.Authenticate(c)
	if err != nil {
		c.Status(403)
		c.Abort()
		return
	}

	var query = db.Order("(random() + 0.6 * random() * (wrong - correct)) DESC")

	if category, useCategory := c.GetQuery("category"); useCategory {
		query = query.Where("category = ?", category)
	}

	q := []Question{}
	if query.Where("last_answer IS NULL OR last_answer < (CURRENT_DATE - INTERVAL '5 MINUTE')").Take(&q, Question{DeckID: c.Param("deck")}).RowsAffected == 0 {
		query.Take(&q, Question{DeckID: c.Param("deck")})
	}

	c.JSON(200, q[0])
}

func questionAnswered(c *gin.Context) {
	_, err := Authentication.Authenticate(c)
	if err != nil {
		c.Status(403)
		c.Abort()
		return
	}

	answer := map[string]interface{}{}
	c.BindJSON(&answer)

	correct := "0"
	wrong := "1"
	if answer["correct"].(bool) {
		correct = "1"
		wrong = "0"
	}
	if err := db.Model(&Question{ID: fmt.Sprintf("%v", answer["questionID"])}).Updates(map[string]interface{}{
		"correct":     gorm.Expr("correct + " + correct),
		"wrong":       gorm.Expr("wrong + " + wrong),
		"last_answer": time.Now(),
	}).Error; err != nil {
		fmt.Println(err)
	}
	c.Status(200)
}

func deleteDeck(c *gin.Context) {
	mail, err := Authentication.Authenticate(c)
	if err != nil {
		c.Status(403)
		c.Abort()
		return
	}
	deck := Deck{}
	db.First(&deck, &Deck{ID: c.Param("deck"), Owner: mail})
	db.Delete(&deck)
	c.Status(200)
}