package internal

import (
	"fmt"
	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/postgres"
	"os"
	"time"
)

type Deck struct {
	ID        string     `json:"id"`
	Owner     string     `json:"-"`
	Name      string     `json:"name"`
	Questions []Question `json:"questions"`
}

type Question struct {
	ID         string    `json:"id"`
	DeckID     string    `json:"-"`
	Question   string    `json:"question"`
	Answer     string    `json:"answer"`
	Category   string    `json:"category"`
	LastAnswer time.Time `json:"lastAnswer"`
	Correct    int       `json:"correct"`
	Wrong      int       `json:"wrong"`
}

var db = getDB()

func getDB() *gorm.DB {
	connStr := fmt.Sprintf("host=%s port=%s user=%s dbname=%s password=%s sslmode=disable", envOrDefault("DB_HOST", "localhost"), envOrDefault("DB_PORT", "5432"), envOrDefault("DB_USER", "postgres"), envOrDefault("DB_NAME", "vocab"), envOrDefault("DB_PASS", "geheim"))
	db, err := gorm.Open("postgres", connStr)
	if err != nil {
		fmt.Printf("Database Error: %s", err.Error())
		os.Exit(3)
	}

	if err := db.AutoMigrate(&Deck{}, &Question{}).Error; err != nil {
		fmt.Printf("Migration Error: %s\n", err.Error())
	}

	fmt.Printf("Migration done\n")
	return db

}

func envOrDefault(env string, alt string) string {
	val, ok := os.LookupEnv(env)
	if !ok {
		return alt
	}
	return val
}
