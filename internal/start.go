package internal

import (
	"codeberg.org/zottelchin/vocab/mailauth"
	"errors"
	"fmt"
	"github.com/gin-gonic/gin"
	"github.com/markbates/pkger"
	"github.com/markbates/pkger/pkging"
	"mime"
	"net/http"
	"net/smtp"
	"path"
	"strings"
	"time"
)

var Authentication *mailauth.MailAuth

func Start() {

	r := gin.Default()

	// Hacky way of initializing SMTP details.
	// Please let's use fig for the configuration at some point!
	// https://github.com/kkyr/fig

	mailServer := envOrDefault("SMTP_SERVER", "")
	mailFrom := envOrDefault("SMTP_FROM", "")
	if mailServer == "" || mailFrom == "" {
		panic(errors.New("SMTP_SERVER and SMTP_FROM must be specified"))
	}

	var mailAccount smtp.Auth
	if envOrDefault("SMTP_USERNAME", "") != "" {
		if envOrDefault("SMTP_PASSWORD", "") != "" {
			mailAccount = smtp.PlainAuth("", envOrDefault("SMTP_USERNAME", ""), envOrDefault("SMTP_PASSWORD", ""), strings.Split(mailServer, ":")[0])
		} else if envOrDefault("SMTP_SECRET", "") != "" {
			mailAccount = smtp.CRAMMD5Auth(envOrDefault("SMTP_USERNAME", ""), envOrDefault("SMTP_SECRET", ""))
		} else {
			panic(errors.New("if SMTP_USERNAME is specified, either SMTP_PASSWORD (for PLAIN auth) or SMTP_SECRET (for CRAM-MD5 auth) must be specified"))
		}
	}

	mailEncryption := envOrDefault("MAIL_ENCRYPTION", "")
	if mailEncryption == "none" || mailEncryption == "off" || mailEncryption == "false" {
		mailEncryption = "none"
	} else if mailEncryption != "" && strings.TrimPrefix(mailEncryption, "insecure-") != "tls" && strings.TrimPrefix(mailEncryption, "insecure-") != "starttls" {
		panic(errors.New("SMTP_ENCRYPTION must be one of the following: none, tls, starttls, insecure-tls, insecure-starttls"))
	}
	if !strings.Contains(mailServer, ":") {
		if strings.HasSuffix(mailEncryption, "starttls") {
			mailServer += ":587"
		} else if strings.HasSuffix(mailEncryption, "tls") {
			mailServer += ":465"
		} else {
			mailServer += ":25"
		}
	} else if mailEncryption == "" && strings.HasSuffix(mailServer, ":587") {
		mailEncryption = "starttls"
	} else if mailEncryption == "" {
		mailEncryption = "tls"
	}
	if mailEncryption == "none" {
		mailEncryption = ""
	}

	Authentication = &mailauth.MailAuth{
		MailServer:     mailServer,
		MailAccount:    mailAccount,
		MailEncryption: mailauth.Encryption(mailEncryption),
		MailFrom:       mailFrom,
		ServiceName:    "Vocab",
		RootURL:        envOrDefault("ROOT_URL", "http://localhost:8000"),
		EndpointPath:   "/api/authorize",
		OnLogin: func(email string) error {
			fmt.Printf("User logged in: %s\n", email)
			return nil
		},
		AllowAtInvalid: true,
		CookieMaxAge:   30 * 24 * time.Hour,
	}
	Authentication.Setup(r)

	Setup(r)

	pkger.Include("/ui/dist")
	r.Use(func(c *gin.Context) {
		requestPath := c.Request.URL.Path
		info, err := pkger.Stat("/ui/dist" + requestPath)
		if err != nil && !strings.HasPrefix(requestPath, "/api/") && !strings.Contains(path.Base(requestPath), ".") {
			// Use Vue app as a fallback
			requestPath = "/"
			info, err = pkger.Stat("/ui/dist" + requestPath)
		}
		var file pkging.File
		if err == nil {
			if info.IsDir() && !strings.HasSuffix(requestPath, "/") {
				c.Redirect(http.StatusFound, requestPath+"/")
				return
			} else {
				if info.IsDir() {
					file, err = pkger.Open("/ui/dist" + requestPath + "index.html")
				} else {
					file, err = pkger.Open("/ui/dist" + requestPath)
				}
			}
		}
		if err == nil {
			c.DataFromReader(200, info.Size(), mime.TypeByExtension(path.Ext(file.Name())), file, map[string]string{})
		} else {
			c.Next()
		}
	})

	panic(r.Run())
}
